import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'

import Container from 'muicss/lib/react/container';
import Button from 'muicss/lib/react/button';

import './index.css';

const makeGrid = (size = 0, makeRandom = false) => {

  let width = 0;
  let height = 0;
  if(size === 0) {
    width = 10;
    height = 10;
  } else if(size === 1) {
    width = 70;
    height = 50;
  } else if(size === 2) {
    width = 100;
    height = 80;
  }

  let grid = [];
  for (let i = 0; i < height; i++){
    let row = [];
    for (let j = 0; j < width; j++){
      let value;
      if (makeRandom){
        value = Math.random() > 0.85;
      }
      row.push({
        status: value ? 1 : 0,
      });
    }
    grid.push(row);
  }
  return grid;
};

export const advanceGrid = function(grid = []){
  let gridHeight = grid.length;
  let gridWidth  = grid[0].length;

  let calculateNeighbours = function(x,y) {
    // since the world is toroidal: if the cell is at the edge of the grid we
    // will reference the cell on the opposite edge
    let topRow =  x - 1 < 0 ? (gridHeight - 1) : x - 1;
    let bottomRow = (x + 1 === gridHeight) ? 0 : x + 1;
    let leftColumn =  y - 1 < 0 ? (gridWidth - 1) : y - 1;
    let rightColumn = (y + 1 === gridWidth) ? 0 : y + 1;

    let total = 0;
    total += grid[topRow][leftColumn].status;
    total += grid[topRow][y].status;
    total += grid[topRow][rightColumn].status;
    total += grid[x][leftColumn].status;
    total += grid[x][rightColumn].status;
    total += grid[bottomRow][leftColumn].status;
    total += grid[bottomRow][y].status;
    total += grid[bottomRow][rightColumn].status;

    return total;
  };
  //apply the rules of the game by comparing with the existing grid to build
  //a new array
  let gameState = [];
  for (let i = 0; i < gridHeight; i++) {
    let row = [];
    for (let j = 0; j < gridWidth; j++) {
      let cellIsAlive = grid[i][j].status;
      let neighbours = calculateNeighbours(i,j);
      if (cellIsAlive) {
        if (neighbours < 2) {
          row.push({ status: 0 });
        } else if (neighbours > 3){
          row.push({ status: 0 });
        } else {
          row.push({ status: 1 });
        }
      }
      if (!cellIsAlive) {
        if (neighbours === 3) {
          row.push({
            status: 1,
          });
        } else {
          row.push({ status: 0 });
        }
      }
    }
    gameState.push(row);
  }
  return gameState;
};

export class GameOfLife extends Component {

  stop() {
    if(this.props.timerId !== 0) {
      clearInterval(this.props.timerId);
      this.props.setTimerId(0);
    }
    this.props.handleStopClick();
  }

  setTimer(newDelay: number) {
    if(this.props.timerId !== 0) {
      clearInterval(this.props.timerId);
    }
    let interval = setInterval(this.props.tick, newDelay);
    this.props.setTimerId(interval);
  }

  run() {

    if(this.props.timerId !== 0) {
      return;
    }
    if(this.props.speed === 0) {
      this.setTimer(1500)
    }
    else if(this.props.speed === 1) {
      this.setTimer(1000)
    }
    else if(this.props.speed === 2) {
      this.setTimer(250)
    }

    this.props.handleRunClick();
  }

  step() {
    this.stop();
    this.props.tick();
    this.props.handleSingleStepClick();
  }

  clear() {
    this.stop();
    this.props.handleClearClick();
  }

  random() {
    this.stop();
    this.props.handleRandomClick();
  }

  setSpeed(newSpeed: number) {

    if(newSpeed === 0) {
      this.props.handleSpeedSlowClick();
      if(this.props.active === true) {
        this.setTimer(1500);
      }
    }
    else if(newSpeed === 1) {
      this.props.handleSpeedMediumClick();
      if(this.props.active === true) {
        this.setTimer(1000);
      }
    }
    else if(newSpeed === 2) {
      this.props.handleSpeedFastClick();
      if(this.props.active === true) {
        this.setTimer(250);
      }
    }
  }

  setSize(newSize) {

    if(newSize === this.props.size) {
      // same size, nothing to do
      return;
    }

    if(this.props.active === true) {
      // if running, stop before changing the board size
      this.stop();
    }

    if(newSize === 0) {
      this.props.handleSizeSmallClick();
    }
    else if(newSize === 1) {
      this.props.handleSizeMediumClick();
    }
    else if(newSize === 2) {
      this.props.handleSizeLargeClick();
    }

    // when changing size add random data
    this.props.handleRandomClick()
  }

  render() {
    const {
      active,
      generation,
      size,
      speed,
      handleToggleCellClick,
    } = this.props

    if(active === true) {
      this.run();
    }

    return (
      <div>
        <div>
          <Container fluid={true} className="mui-container">
          <Button variant="raised" className={active ? "mui-btn--accent" : ""} onClick={() => this.run()}>  Run</Button>
          <Button variant="raised" className={active ? "" : "mui-btn--accent"} onClick={() => this.stop()}>Stop</Button>
          <Button variant="raised" onClick={() => this.step()  }> Step   </Button>
          <Button variant="raised" onClick={() => this.clear() }> Clear  </Button>
          <Button variant="raised" onClick={() => this.random()}> Random </Button>
          </Container>
        </div>
        <Container fluid={true} className="mui-container">
          <div>
            <span>Generation: {generation}</span>
          </div>
        </Container>

        <Container >
          <div className="fixwidth">
            {this.props.grid.map((row,i) =>
              <div className="newrow" key={i}> {row.map((cell,j) =>
                <div
                  onClick={() => handleToggleCellClick(i,j)}
                  key={j}
                  className={cell.status === 0 ? "boxempty" : "boxfull" }
                >
                </div>
              )}
            </div>
            )}
          </div>
        </Container>

        <div className="newrow">
          <Container fluid={true} className="mui-container">
            <span> Board Size: </span>
            <Button variant="raised" className={size === 0 ? "mui-btn--accent" : ""} onClick={() => this.setSize(0)}> 10x10</Button>
            <Button variant="raised" className={size === 1 ? "mui-btn--accent" : ""} onClick={() => this.setSize(1)}> 70x50</Button>
            <Button variant="raised" className={size === 2 ? "mui-btn--accent" : ""} onClick={() => this.setSize(2)}>100x80</Button>
          </Container>
        </div>
        <div>
          <Container fluid={true} className="mui-container">
            <span> Speed: </span>
            <Button variant="raised" className={speed === 0 ? "mui-btn--accent" : ""} onClick={() => this.setSpeed(0)}>1500 ms</Button>
            <Button variant="raised" className={speed === 1 ? "mui-btn--accent" : ""} onClick={() => this.setSpeed(1)}>1000 ms</Button>
            <Button variant="raised" className={speed === 2 ? "mui-btn--accent" : ""} onClick={() => this.setSpeed(2)}> 250 ms</Button>
          </Container>
        </div>
      </div>
    )
  }
}

// Action
const runAction = { type: 'run' };
const stopAction = { type: 'stop' };
const singleStepAction = { type: 'singleStep' };
const clearAction = { type: 'clear' };
const randomAction = { type: 'random' };
const sizeSmallAction = { type: 'sizeSmall' };
const sizeMediumAction = { type: 'sizeMedium' };
const sizeLargeAction = { type: 'sizeLarge' };
const speedSlowAction = { type: 'speedSlow' };
const speedMediumAction = { type: 'speedMedium' };
const speedFastAction = { type: 'speedFast' };
const tickAction = { type: 'tick' };

function toggleCellAction(x, y) {
  return { type: 'toggleCell', x, y, };
}

const initialState = {
  active: true,
  timerId: 0,
  singleStep: false,
  clear: false,
  random: false,
  generation: 0,
  size: 0,
  speed: 0,
  grid: makeGrid(0, true),
};

function gameOfLife(state = initialState, action) {

  switch (action.type) {
    case 'clear':
      return {
        ...state,
        clear: true,
        generation: 0,
        grid: makeGrid(state.size),
      };
    case 'run':
      return {...state, active: true,  singleStep: false, clear: false, random: false };
    case 'stop':
      return {...state, active: false, singleStep: false, clear: false, random: false };
    case 'singleStep':
      return {
        ...state,
        singleStep: true,
      };
    case 'sizeSmall':
      return {...state, size: 0 };
    case 'sizeMedium':
      return {...state, size: 1 };
    case 'sizeLarge':
      return {...state, size: 2 };
    case 'speedSlow':
      return {...state, speed: 0 };
    case 'speedMedium':
      return {...state, speed: 1 };
    case 'speedFast':
      return {...state, speed: 2 };
    case 'tick':
      return {
        ...state,
        generation: state.generation + 1,
        grid: advanceGrid(state.grid.slice(0)),
      };
    case 'setTimerId':
      return {...state, timerId: action.timerId };
    case 'random':
      return {
        ...state,
        random: true,
        generation: 0,
        grid: makeGrid(state.size, true),
      };
    case 'toggleCell':
      let board = state.grid.slice(0);
      let cell = board[action.x][action.y];
      cell.status = cell.status === 1 ? 0 : 1;
      return {
        ...state,
        grid: board,
      }
    default:
      return state;
  }
}

// Store
const store = createStore(gameOfLife)

// Map Redux state to component props
function mapStateToProps(state) {
  return {
    active: state.active,
    singleStep: state.singleStep,
    clear: state.clear,
    random: state.random,
    generation: state.generation,
    size: state.size,
    speed: state.speed,
    timerId: state.timerId,
    grid: state.grid,
  }
}


function setTimerId(timerId) {
  return {
    type: 'setTimerId',
    timerId: timerId,
  };
}


// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    setTimerId: (timerId) =>  dispatch(setTimerId(timerId)),
    handleRunClick: () => {  dispatch(runAction)},
    handleStopClick: () => dispatch(stopAction),
    handleSingleStepClick: () => dispatch(singleStepAction),
    handleClearClick: () => dispatch(clearAction),
    handleRandomClick: () => dispatch(randomAction),
    handleSizeSmallClick: () => dispatch(sizeSmallAction),
    handleSizeMediumClick: () => dispatch(sizeMediumAction),
    handleSizeLargeClick: () => dispatch(sizeLargeAction),
    handleSpeedSlowClick: () => dispatch(speedSlowAction),
    handleSpeedMediumClick: () => dispatch(speedMediumAction),
    handleSpeedFastClick: () => dispatch(speedFastAction),
    handleToggleCellClick: (x, y) => dispatch(toggleCellAction(x,y)),

    tick: () => { dispatch(tickAction) },
  }
}

// Connected Component
const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(GameOfLife)


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

